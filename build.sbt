sbtPlugin := true

name := """Sbt Commons"""

organization := """io.morgaroth"""

version := "0.19"

crossSbtVersions := Vector("0.13.16", "1.1.6")

enablePlugins(BuildInfoPlugin)

buildInfoKeys := Seq[BuildInfoKey](version)

buildInfoPackage := "io.morgaroth.sbt.commons"

bintrayVcsUrl := Some("https://gitlab.com/morgaroth/sbt-commons")

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))